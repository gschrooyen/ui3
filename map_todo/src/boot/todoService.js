import axios from 'axios'

const RECOURCE_PATH = 'http://localhost:3000/todos'

export default class TodoService {
  get () {
    return axios.get(RECOURCE_PATH).then(result => result.data)
  }
  getDetail (id) {
    return axios.get(RECOURCE_PATH + '/' + id).then(result => result.data)
  }

  delete (todo) {
    return axios.delete(RECOURCE_PATH + '/' + todo.id).then(result => result.data)
  }

  add (todo) {
    return axios.post(RECOURCE_PATH, todo)
  }

  update (todo) {
    return axios.patch(RECOURCE_PATH + '/' + todo.id, todo)
  }
}
